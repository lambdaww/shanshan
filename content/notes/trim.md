---
title: "trim"
date: 2021-02-01
hidden: true
---

```ocaml

(** ----------------------------------------------------  
The function trim takes a int for required bytes and a cache; 
and, remove elements in cache recursively until the free space 
of given cache satisfies the required needs. Elements will be 
removed as in FIFO policy. *)

(** ----------------------------------------------------
Since the trim function is the keystone of the 
size limitation mechanism of cache, the following 
comments for trim function showing a Hoare logic 
style reasoning which can be removed after 
some proper reviewing. 

Given: 
    [f] := current Free cache size
    [r] := Required length (of bytes)
    [m] := the Max number of cache size 
    [size(x)] := the byte size of given x
we have properties:
    [LEMMA1] := { ∀ k. ∃ v. k ∈ queue ↔ (k,v) ∈ map }
    [LEMMA2] := { ∀ (_,v) ∈ map . [f] + [size(v)] <= [m] }
    [INIT] := { 0 <= [f] < [r] <= [m] }
    [GOAL] := { 0 < [r] <= [f] <= [m] }
*)
let rec trim (required_size : int) t : t =
    match FQueue.take_back t.kqueue with
    | None ->
    (** [C0] := { [f] = [m] ← empty cache } *)
        (** { ⊥ ← [LEMMA1] ∧ [C0] } *)
        {empty with max_size = t.max_size}
        (** { [OGAL] } *)
    | Some (kqueue, k) -> 
    (** [C1] := { ∃ k . k ∈ queue } *)
        match Map.find_opt k t.content with
        | None ->  
        (** [C2] := { (k,_) ∉ map } *)
            (** { ⊥ ← [LEMMA1] ∧ [C2] ∧ [C1] } *)
            {empty with max_size = t.max_size}
            (** { [OGAL] } *)
        | Some v -> 
        (** [C3] := { ∃ v . (k,v) ∈ map } 
            [size(v)] := byte length of v *)
            (** [INIT] ∧ [LEMMA2]
            ⇒   { { 0 <  [r] ∧ [f] + [size(v)] <= [m] } 
                ∨ { 0 <= [f] + [size(v)] ∧ [r] <= [m] } } *)
            let remnant = t.remnant + Bytes.length v in
            let content = Map.remove k t.content in
            (** [P] := { { 0 <  [r] ∧ [f] <= [m] } 
                       ∨ { 0 <= [f] ∧ [r] <= [m] } } *)
            if Compare.Int.(remnant >= required_size) 
            then  (** [Bt] := { [r] <= [f] } *)
              (** { [P] ∧ [Bt] } *)
              {t with remnant; content; kqueue} 
              (** { [GOAL] } *)
            else (** [Bf] := { [f] < [r] } *)
              (** { [INIT] ← [P] ∧ [Bf] } *)
              trim required_size {t with remnant; content; kqueue}
              (** { [GOAL] } *)
```
