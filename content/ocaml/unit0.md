---
title: "⟦0⟧️ Le Prélude"
---

# Compilable unit

ocaml 的基本編譯單位 (compilable unit) 是 _**module**_，包含了幾種形式：

- module definition
  - 用 modue expression 來描述
  - 即 `*.ml` 檔案
- module specification
  - aka module type definitions
  - 即 `*.mli` 檔案

這裡目前只關心基本的 ocaml 使用方式，所以我們先來看 modue expression。 ocaml 使用手冊中關於 [module-expr](https://caml.inria.fr/pub/docs/manual-ocaml/modules.html#module-expr) 的定義如下：

```
modue-expr ::= module-path
  | struct [ module-items ] end
  | functor ( module-name : module-type ) -> module-expr
  | module-expr ( module-expr )
  | ( module-expr )
  | ( module-expr : module-type )
```

其中明顯只有第二條規則是真的讓我們可以寫出 module 定義的。而 module 定義實際上就是一個 [module-items](https://caml.inria.fr/pub/docs/manual-ocaml/modules.html#module-items) 夾在 `struct` 和 `end` 之間。

「嗯？可是我打開一個 `*.ml` 檔案以後沒有看到 `struct` 呀?」

是的，一個 `*.ml` 檔案已經預設是一個完整的 module definition，因此整個檔案的內容都相當於已經放在 `struct`-`end` 之間；而且，這個 module 的名字就是檔案名稱（不過 module 的名稱的字首會是大寫）。例如說，創個檔案: `mycode.ml`，那麼我們就有一個 module 叫做 **Mycode**。而這個檔案實際上就是一個 [module-items](https://caml.inria.fr/pub/docs/manual-ocaml/modules.html#module-items)。

## module-items

```
module-items ::= {;;} (definition | expr) {{;;}(definition | ;; expr)} {;;}
```

一組 [module-item**s**](https://caml.inria.fr/pub/docs/manual-ocaml/modules.html#module-items) 是一串的 _module-item_；而一個 module-item 是一個 [definition](https://caml.inria.fr/pub/docs/manual-ocaml/modules.html#definition) 或一個 [expr](https://caml.inria.fr/pub/docs/manual-ocaml/expr.html#expr)。module-item**s** 用 `;;`, double semicolon, 來區別各個不同的 module-item。

- ⚠️ 注意，定義中的 `{;;}` 表示這組 `;;` 是非必要的；所以，其實它**必要**的位置非常地微妙。
- ⚠️ 注意， `;;` 和 `;` 是兩回事：
  - `;;` 是用以區分 module-item 的，其前後可能是 definition 或 expr；
  - `;`, 稱為 _sequence_, 是用來連接兩個不同的 expr 用的： `expr := expr ; expr`。

## module-item

一個 module-item 是一個 [definition](https://caml.inria.fr/pub/docs/manual-ocaml/modules.html#definition) 或是一個 [expr(ession)](https://caml.inria.fr/pub/docs/manual-ocaml/expr.html#expr)。建議點連結進去看一下他們的定義有哪些。

- 一個 [definition](https://caml.inria.fr/pub/docs/manual-ocaml/modules.html#definition) 可以被簡單地分類成以下幾種：
  - `let-binding`
  - data types
    - type deifinition
    - exception deifinition
  - module
    - `module` and `module type`
    - `open` and `include`
  - class definition
- 一個 [expr(ession)](https://caml.inria.fr/pub/docs/manual-ocaml/expr.html#expr) 也可以被簡單分類成以下幾種：
  - value/data contructors
  - type declare (including _coercion_)
  - built-in operators
  - function (_definition_ and _application_)
  - statement (_branch_, _loop_ and _sequence_)
  - pattern matching
  - (local) `let-binding`
  - misc: `lazy`, object, etc..

# Try first

這章節會介紹一些我們應該先知道的關於 expr 最基本的事情。

## ocaml's REPL

OCaml 的 REPL (**R**ead–**E**val–**P**rint **L**oop) 可以吃的不是一個 module-item**s** 或 module-item，而是另外定義的 _toplevel-input_。主要的差別有二：(1) 如果是 expr ，一次只能吃一組；(2) 結尾一定要加一組 `;;`。

```
toplevel-input ::= { definition }+ ;;
                 ∣  expr ;;
                 ∣  # ident [ directive-argument ] ;;
```

⚠️ 如果一次用 `;;` 來輸入多項 definition 和/或 expr，那只會有第一項有效。

```ocaml
# let x = 1 ;; let y = 2 ;;
(* val x : int = 1 🤔 *)

# type t1 = T1 ;; type t2 = T2 ;;
(* type t1 = T1 🤔 *)

# let x = 3
  let y = 1
  ;;
(* val x : int = 3
   val y : int = 1 *)

# type t1 = T1
  type t2 = T2
  ;;
(* type t1 = T1
   type t2 = T2 *)
```

## let-binding

let-binding 很神奇地同時出現在兩個地方： [definition](https://caml.inria.fr/pub/docs/manual-ocaml/modules.html#definition) 和 [expr(ession)](https://caml.inria.fr/pub/docs/manual-ocaml/expr.html#expr)。

```
definition ::= ...
  |  let [rec] let-binding { and let-binding }
expr ::= ...
  |  let [rec] let-binding { and let-binding } in expr
```

- `rec` 用來標記此段 let-binding 是 recursive definition
- `and` 用來順便一起宣告或定義其他 let-binding
- ⚠️ 關於對應 `let` 的結尾
  - 在 **expr** 中， 需要一個相對應的 `in` 來結尾，並引入接續的那個 expr；
  - 在 **definition** 中，不需要那個 `in`，而是可以用 `;;` 來做 module-item 的平行宣告。

而 let-binding 本身的定義也不複雜，只有三種情況：**pattern matching**, **function introduciton** 和 **typed variable introduction**。其中 function introduction 需要的 parameter 可以是 _pattern_ 或是 _label_。

```
let-binding ::= pattern =  expr
  ∣  value-name { parameter } [: typexpr] [:> typexpr] = expr
  ∣  value-name : poly-typexpr = expr
```

一些簡單的例子如下：

```ocaml
let (x, y) = foo ()
and (Some x) = getFrom somewhere

let plus x y = x + y
and switch {~flag:None} =
  match flag with
  | None -> 0
  | Some n -> n

let x : int = 1 + 1
and y : bool = x >= 2
```

## pattern matching

OCaml 的 (explicitly) [**pattern matching**](https://caml.inria.fr/pub/docs/manual-ocaml/expr.html#pattern-matching) 定義在 [expr](https://caml.inria.fr/pub/docs/manual-ocaml/expr.html) 之中，透過 `match .. with ..` 或是 `function ..` 來使用。而一組 pattern matching 本身是由至少一組的 "**case**" 組成，其中第一組 case 前面的 bar, `|`, 可以省略。

```
expr ::= ...
  ∣  match expr with  pattern-matching
  ∣  function pattern-matching

pattern-matching ::=
  [ | ] pattern  [when expr] ->  expr
  { |   pattern  [when expr] ->  expr }
```

```ocaml
let f1 x =
  match flag with
  | None -> 0
  | Some n -> n
and f2 = function | None -> 0 | Some n -> n
(* f1 and f2 are identical *)
```

⚠️ 一組 single case 的 pattern matching，或說是直接使用 pattern 的情況，則是只出現在 [**let-binding**](https://caml.inria.fr/pub/docs/manual-ocaml/expr.html#let-binding) 和 [**parameter**](https://caml.inria.fr/pub/docs/manual-ocaml/expr.html#parameter) 這兩個地方。而 paramter 則分別在 _expr_ 和 _let-binding_ 中各有一個地方會用到。

```
let-binding ::= ...
  | pattern =  expr

parameter ::= pattern

expr ::= ...
  | fun { parameter }+ [ : typexpr ] -> expr
let-binding ::= ...
  | value-name  { parameter }  [: typexpr]  [:> typexpr] =  expr
```

總結來說， pattern matching 總計只會出現在：
- 明確的使用 _pattern matching_ 時
  - `match .. with ..`
- 使用 _let-binding_ 來定義變數
  - `let (C x y) = f z`
- 使用 _let-binding_ 或 `fun` 或 `function` 來定義函數
  - `let curry f (x, y) = f x y`
  - `let curry f = fun (x, y) -> f x y`
  - `let curry f = function | (x, y) -> f x y`

## capitalized name

- capitalized
  - module type name ⚠️
  - module name
  - data constructor name
  - tag name
- lowercase
  - module type name ⚠️
  - value name
  - type constructor name
  - class name
  - instance variable name
  - method name
