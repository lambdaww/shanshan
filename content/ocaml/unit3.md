---
title: "⟦3⟧️ Object(ive)"
---

...


```ocaml
(* immediate object *)
let y =
  object
    val mutable x = 0
    method get_x = x
    method edit new_x = x <- new_x
  end
;;

class point =
  object
    val mutable x = 0
    method get_x = x
    method edit new_x = x <- new_x
  end
;;
```
