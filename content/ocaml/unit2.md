---
title: "⟦2⟧️ Polymorphic Variants"
---

# tl;dr

- The polymorphic variant type, aka polymorphic variants
  - to define anonymous ADT on the fly
  - to ad-hoc control ADT (to expend or to refine)
  - using Tag instead of Constructor
  - based-on subtyping relation

# basic syntax

```ocaml
    type    t0 = [ `Tag1 | `Tag2 ]
    type    t1 = [ `Tag1 | `Tag2 ]
    type 'a t2 = [ `Tag1 of 'a -> 'a ]
    type 'a t3 = [ `Tag1 of { f1 : 'b . 'a -> 'b } ] (* 💀 : record *)
    type 'a t4 = [ `Tag1 of < m1 : 'b . 'a -> 'b > ] (* 👌 : method *)
```

- 3 variant types
  - open (expendable) polymorphic variant type: [> t ]
  - closed (refinable) polymorphic variant type: [< t ]
  - exact (fixed) polymorphic variant type: [ t ]

## define via type

```ocaml
(* exact poly variant *)
type t0 = [ `A | ` B]

(* open/closed poly variant *)
type 'a t1 = [< `A | `B ] as 'a
type 'a t1 = [> `A | `B ] as 'a
```

## define via pattern

```ocaml
let foo x = function
  | `A -> x
  | `B a -> a
  | `C f -> f x
(* val foo :  'a
           -> [< `A | `B of 'a | `C of 'a -> 'a ]
           -> 'a
           =  <fun> *)
```
- why `[< ..` ?

```ocaml
let foo x = function
  | `A -> x
  | `B a -> a
  | any -> any
(* val foo : ([> `A | `B of 'a ] as 'a)
           -> 'a
           -> 'a
           =  <fun> *)
```

- why `[> ..` ?  `as` `'``a` ? how come the pvt go up to the 1st place?

## closed type only!

- conjunctive tag [< `U of int & string ]
- the `>` inside closed type… C’est quoi?
![](https://paper-attachments.dropbox.com/s_A0C1375B476182A35972C49ACDC6937885F00C288BF973ED777ED90F8FBF94CC_1600839373655_image.png)

## subtyping

- subtyping au OCaml.

> Given type $$t_1$$ and $$t_2$$, subtyping relation: $$t_1 \preceq t_2$$ holds
> if all of defined Tags (and their associate types) in $$t_1$$ can be found in $$t_2$$.

- coercion

> Given type $$t_1$$ and $$t_2$$,
> $$t_1$$ can be coreced into $$t_2$$, denoted $$t_1$$:> $$t_2$$, if $$t_1 \preceq t_2$$.

- in OCaml, it’s an expr

```ocaml
expr ::= ..
| ( expr :> typexpr )
| ( expr :  typexpr :> typexpr )
```

- tag weakening (using or-pattern)

```ocaml
    let trans = function
    | `A1 | `A2 -> `A1
    | `B1 | `B2 -> `B1 ;;
    (* val trans : [< `A1 | `A2 | `B1 | `B2 ] -> [> `A1 | `B1 ] = <fun> *)
    - 😵   `[< .. ] -> [> .. ]`  why?
```

- type abbreviation
    - given `type t = [..]`
    - `#t` means all tag patterns of type `t`

```ocaml
type t1 = [ `A | `B ]
type t2 = [ `C | `D ]
let foo = function
| #t1 -> 0
| #t2 -> 1 ;;
(* val foo : [< `A | `B | `C | `D ] -> int = <fun> *)
```

## shortcoming

- lack of enough static type information for compile-time optimization
- weaker type principle
    - say, no totally checking on pattern matching
- increasing complicity of type info

## when to use it?

- Polymorphic variant types will be required if a subtyping is required; i.e.,
    - you want to make a partial function to be a total function; or, let’s put it another way,
    - you just want to process only a subset of a data type; or, in another direction,
    - you want to process several types at once.
- It’s replaceable by GATD since GADT provides greater power.

# 🤔

## example 1

Given following code:

```ocaml
type stopwatch =
  [ `Running of int
  | `Paused of int ]

let start = `Running 0
let tick (`Running s) = `Running (s + 1)
let pause (`Running s) = `Paused s
let resume (`Paused s) = `Running s
```

OCaml shows that:

```ocaml
val start : [> `Running of int ] = `Running 0
val tick : [< `Running of int ] -> [> `Running of int ] = <fun>
val pause : [< `Running of 'a ] -> [> `Paused of 'a ] = <fun>
val resume : [< `Paused of 'a ] -> [> `Running of 'a ] = <fun>
```

Namely,

- value start has type of something more expressive than [ `Running of int ]
- function tick
    - requires something less expressive than [ `Running of int ]
    - returns something more expressive than [ `Running of int ]
- function pause
    - requires something less expressive than [ `Running of ‘a ]
    - returns something more expressive than [ `Running of ‘a ]
- function resume requires
    - requires something less expressive than [ `Paused of ‘a ]
    - returns something more expressive than [ `Runninpg of ‘a ]
- 神秘事件

```ocaml
let x = start |> tick |> pause |> tick ;;
(* 💀 Error: This expression has type [< `Running of int ] -> [> `Running of int ]
       but an expression was expected of type [> `Paused of int ] -> 'a
       The first variant type does not allow tag(s) `Paused *)

let f = pause |> resume ;;
(* 💀 Error: This expression has type
         ([< `Paused of 'b ] as 'a) -> [> `Running of 'b ]
       but an expression was expected of type
         (([< `Running of 'd ] as 'c) -> ([> `Paused of 'd ] as 'e)) -> 'f
       Type [< `Paused of 'b ] as 'a is not compatible with type 'c -> 'e *)

let f x = x |> pause |> resume ;;
let f x = pause x |> resume ;;
let f = function `Running _ as x -> pause x |> resume ;;
(* val f : [< `Running of 'a ] -> [> `Running of 'a ] = <fun> *)
```

# misc

```
tag-spec ::= TagName, (product of types)
tag-spec+ ::= TagName, ((product of types) or (type union))
```
