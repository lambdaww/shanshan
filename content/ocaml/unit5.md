---
title: "⟦5⟧️ Module"
---

# module

- definitions
  - Simple: Value | Type | Exception
  - Class: def. | Type
  - Module
  - Module type
  - Opening/Including

## define module

```ocaml
module M = struct
<definitions>
end;;
```

## declare module

```ocaml
module type M = sig
<specifications>
end;;
```

## using module

- loading
    - `open M` symbolic loading (identity reading)
        - shadowing other identity within same scope
        - `let .. in` or `M.(expr)` can be used for locally opening
    - `include M` literal loading (code cloning)
- typing
    - `module M1 = (M2 : Mt)`
    - `module M1 = (struct … end : Mt) ;;`
    - `module M1 : Mt = struct … end ;;`
- specification expending
    - within any `sig`, using `include M` can load every spec of `M`
    - and therefore it’s kind of a way of expending `M`

# Functor

- ..
- ..
