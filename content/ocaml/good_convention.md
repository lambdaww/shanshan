---
title: "good convention"
date: 2021-02-17
hidden: false
---

# the principles

## locally opened principle

prefer 

```ocaml
let res = 
  let open M in 
  moo ()
```

over

```ocaml
let res = M.moo () 
```

## pre-define comment principle

use 

```ocaml
(* ... *)
let foo = ()
```

rather than 

```ocaml
let foo = ()
(* ... *)
```
## scoped branch principle

use 

```ocaml
if p then (
  exp1
) else (
  exp2
)
```

or 

```ocaml
if p then 
  begin
    exp1
  end else begin
    exp2
  end
```

or 

```ocaml
if p then exp1 else exp2
```

## returned type

return `'t option` is better than exception. If it’s unavoidable, name the function with `_exn` as its suffix.


## type t in module 

if a module reflectively introduces a data structure, the very type reflects that structure should be named as `t`. And it’s preferred to be the first argument (of its module).


## labelled parameter

When a function takes more than one parameters with the same type, those parameters should be labelled in different names.

```ocaml
let foo : x:int -> y:int -> unit
```

## avoid irrefutable matcher (the wildcase)

Avoid `| _ -> ..` and `| x -> ..` if possible. Thus, if the matched structure is modified, compiler will warn you about it.


## avoid more-than-3-element tuple

Whenever you need to put more than 3 elements together, use record over tuple. 


## refactoring with let

Use `let` to introduce simple variable and thus build tuple with them. Namely, prefer

```ocaml
let x = a + b in
let y = a - b in
let z = a + a + b in
(x, foo y, goo z x)
```

over 

```ocaml
(a + b, foo (a - b) , goo (a + a + b) (a + b))
```

# ocamldoc

