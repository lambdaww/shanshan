---
title: "lens in ocaml"
date: 2021-02-24
hidden: false
---

有名的 lens 是一套在語法上簡化存取 record type 裡面的 data 的機制。
原本是在 Haskell 中相當好用，但是隨著知名度漸增，其他語言，尤其是其他 functional 語言也開始逐漸支援。

Haskell 中的 lens 可以用 typeclass 來簡單達成，但是在 OCaml 中沒有那麼方便，不過其實也還是可以用 module 來達到類似的效果。
目前，在 opam 之中已經有一套被收入的實作版本：[ocaml-lens](https://github.com/ocaml/opam-repository/tree/master/packages/lens)。
這一版其實沒有很強大複雜，不過基本的 get/set 以及 infix operators 都有提供。
更進一步還有提供 deriving (via ppx) 可以實現自動推導出給定 record type 的相對應 lens 定義。

## install

安裝基本上就是靠 opam 就好: 

```
opam install lens
```

不過最好確認一下 ocaml 版本以及 ppx dependencies。我現在確認可以的情況是：

```
ocaml                   4.09.1      The OCaml compiler (virtual package)
ocaml-base-compiler     4.09.1      Official release 4.09.1
ppx_deriving            4.5         Type-driven code generation for OCaml >=4.
ppx_tools               6.3         Tools for authors of ppx rewriters and oth
ppxfind                 1.4         Tool combining ocamlfind and ppx
```

這篇文章在寫的時候用的是 1.2.3，雖然說 1.2.4 已經出了，但是 compile 不過。

## import

基本款要 import `Lens` 進來，但是如果要用 infix operators，就要順勢 import `Infix` module。
搞笑的一點是，目前我沒有看到不用 infix 就可以 `get` 的方法，
可能是我個人對 ocaml 和這個 lens 都還不夠熟。

```
open Lens
open Lens.Infix
```

## simple example

```ocaml
open Lens
open Lens.Infix
open Format

type rr = { ra : ra; rb : rb}
and ra = { i : int; j : int}
and rb = { x : string; y : bool } ;;

let r = { ra = { i = 1; j = 2};
          rb = { x = "x"; y = false }} ;;

let ra_len = {
    get = (fun x -> x.ra);
    set = (fun v x -> { x with ra = v });
  };;

let i_len = {
    get = (fun x -> x.i);
    set = (fun v x -> { x with i = v });
  };;

let get_i =
  i_len --| ra_len ;;

let main = begin
    printf "%d" (r |. get_i)
    (*print_endline "Hello, world!"*)
  end;;

main;;
```
