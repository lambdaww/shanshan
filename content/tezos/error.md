---
title: "error monad"
date: 2021-01-05
categories: []
hidden: false
---

There are two common monads used crossing almost entire codebase. The one is Lwt, an continuation monad with thread control underneath; and, the other is, well, let’s just call it Err for now. The monad Err is an error monad that means it allows user to raise an error message without worrying about how is message will be handled underneath.

## Working with Err or Lwt

To use either of them is relative simple, one can easily define and use the following traditional lifting and binding:


```haskell
-- purely Lwt
(>>=) :: Lwt a -> (a -> Lwt b) -> Lwt b
(>|=) :: Lwt a -> (a ->     b) -> Lwt b

-- purely Err
(>>?) :: Err a -> (a -> Err b) -> Err b
(>|?) :: Err a -> (a ->     b) -> Err b
```

Additionally there are also some pre-defined returns for Err.

```haskell
-- basic return
ok :: a -> Err a

-- suger
ok_unit   = ok ()
ok_none   = ok None
ok_some x = ok (Some x)
ok_nill   = ok []
ok_true   = ok True
ok_false  = ok False
```

## Working Together

To use them at same time will be a tricky case. Consider two given monads, M and N, the composed monads (M N) and (N M) could be entirely different. They may or may not share same mathematical properties. Luckily, for our use case, we only need to consider Lwt (Err a). Let’s give it an aux name for convenience as well.

```haskell
monad Lwt a
monad Err a
monad Mix a = Lwt (Err a)
```

Now, to work with Mix monad, one will naturally define its lifting, binding and return:

```haskell
-- Mix -> Mix
(>>=?) :: Mix a -> (a -> Mix b) -> Mix b
(>|=?) :: Mix a -> (a -> b) -> Mix b

-- basic return
return :: a -> Mix a

-- sugar
return_unit   = return ()
return_none   = return None
return_some x = return (Some x)
return_nil    = return []
return_true   = return True
return_false  = return False
```

And, for the situation where one want to eval a Mix result from a Err value. There are also two pre-defined lifting-binding operators:

```haskell
-- Err -> Mix
(>>?=) :: Err a -> (a -> Mix b) -> Mix b
(>|?=) :: Err a -> (a -> Lwt b) -> Mix b
```

# More detail for error itself

## The result

In OCaml, the module Result is part of the stdlib and it is where the structure `result` is defined. The `result` is actually an alias of Either in Haskell. That is to say, this `result` structure is actually a functor as well as a monad.

```haskell
type ('a, 'b) result = Ok of 'a | Error of 'b
```

This `result` structure reflects the simplest error monad which requires no extra property on the error message, namely the `b` in above definition.

In this use case, one would prefer that an error message is not just a simple “this is ooo error” but the whole trace from the surface to the deep place where an error occurs.

Based-on this, Lwt provides two sugars as the return function for a Lwt-Result transformer.

```ocaml
val return_ok : 'a -> ('a, 'b) Result.result t
val return_error : 'e -> ('a, 'e) Result.result t
```

In proto_environment, we have the definition of tzresult, which is a type wrapper of result.

```ocaml
type 'a tzresult = ('a, error trace) result
```

where, we know that,

```ocaml
type ‘a trace = ‘a list
```

and `error` is a co-product type over error constructors, which can be defined by `type error = ..` and `error += NewError`.

---

In Environment_V1, we also have an error casting function

```ocaml
val wrap_error : 'a Error_monad.tzresult -> 'a tzresult

type error += Ecoproto_error of Error_monad.error
```

## From Err to Trace

The Err monad is actually defined by a pair of return value and underneath error information. This can be illustrated as follow

... 懶

```ocaml
error :: Traceable e => e -> Err a
fail :: Traceable e => e -> Mix a
```

(‘a, ‘e trace) result
