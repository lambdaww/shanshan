---
title: "error monad"
date: 2021-01-05
categories: []
hidden: false
---

# prelude
- From *2.1 Mathematical representation* in the whitepaper
    - A blockchain protocol is fundamentally a monadic implementation of 
        concurrent mutations of a global state
| i.e. a blockchain (protocol) is just a state monad roughly speaking |

    - blocks are operators over a global state
    - a block $$b$$ is defined by $$b \in B \subset S^{S\cup\{\oslash\}}$$ where $$B^A \equiv \{A \to B\}$$
        - namely $$\forall b \in B . b :: S\cup\{\oslash\} \to S$$
- From *2.3 Functional representation* in the whitepaper
    - The state is represented with the help of a Context module 
        which encapsulates a disk-based immutable key-value store.
        - implemented via the asynchronous monad Lwt
- From [The big abstraction barrier](https://tezos.gitlab.io/developer/entering_alpha.html#the-big-abstraction-barrier-alpha-context), **Alpha_context** defines 
    - underlying connection to concrete storage structure (eventually, Irmin)
    - global state, `Alpha_context.t`, and related functions
    - [alpha_context.ml](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/alpha_context.ml) :
        - introducing everything which can be stored in context as modules
        - the real structure type underneath is defined in [raw_context](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/raw_context.ml) as `t`, 
            in other words, you can find the concrete definition of context in [here](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/raw_context.ml#L28)
# 📖 to read protocol
- [How to start reading protocol Alpha](https://tezos.gitlab.io/developer/entering_alpha.html) (tezos.gitlab.io)
- [tezos protocol upgrade development](https://research.metastate.dev/tezos-protocol-upgrade-development-part-1/) (metastate)
- [How to wrte a Tezos protocol](https://blog.nomadic-labs.com/how-to-write-a-tezos-protocol.html) ([part 2](https://blog.nomadic-labs.com/how-to-write-a-tezos-protocol-part-2.html)) (nomadic lab)

Starting with [How to start reading protocol Alpha](https://tezos.gitlab.io/developer/entering_alpha.html#how-to-start-reading-protocol-alpha) which divides the protocol by a referential module - [Alpha_context](https://tezos.gitlab.io/developer/entering_alpha.html#the-big-abstraction-barrier-alpha-context) ([ml](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/alpha_context.ml)/[mli](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/alpha_context.mli)). 
https://tezos.gitlab.io/developer/entering_alpha.html


# 🖇️ Extra
## Z and Zarith

Sometimes you can find a module `Z` or a type `Z.t` which is not defined anywhere locally. They are in fact loaded from the [Zarith](https://github.com/ocaml/Zarith) which presents arbitrary-precision integers and is [one of tezos dependencies](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/lib_stdlib/tezos-stdlib.opam). 


----------
# Convention
## module function
    module Make_Something (M0 : MT0) (M1 : THE_OTHER.T) : Something with t = Another.t



- In Storage
    - module **Contents** uses **Storage_functors**.*Make_indexed_carbonated_data_storage*
    - module **Indexed_context** uses 
- **Make_indexed_carbonated_data_storage** [←Storage_functors] returns type = 
    **Non_iterable_indexed_carbonated_data_storage** [←Storage_sigs]
- Make_carbonated_map_expr [←Storage]



- gas costing 
- src/proto_alpha/lib_protocol/storage_sigs.ml
    - 


    - [module Make_indexed_carbonated_data_storage](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/storage_functors.ml#L385) 
    - The “storage” 📦 introduced in Storage_sigs ([mli](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/storage_sigs.ml)) and Storage_functors ([ml](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/storage_functors.ml)/[mli](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/storage_functors.mli)) is a universal data structure along with its operations (namely, not just the storage in/for SC) 
    - To add caching upon storage, there are two 
        [ ]  [👀 **ToBeRead**] it seems the only place we can store cached data will be Context which, 
            be more specific, is the Raw_context ([ml](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/raw_context.ml)/[mli](https://gitlab.com/metastatedev/tezos/-/blob/proto-proposal/src/proto_alpha/lib_protocol/raw_context.mli)).
        [ ] [👀 **ToBeRead**] to pin point where we can access cache instead of Irmin, 
            understand the implementation within NIICPS (Non_iterable_indexed_carbonated_data_storage) will be necessary.
        [ ] [👀 **ToBeRead**] probably also requiring on reading [irmin](https://paper.dropbox.com/doc/Cache-Mechanism--A_i8OjWwRQ9Wn5dq9xSyT_e7Ag-a6UdhrbB2N52jwpkzTQGV#:uid=804278867688295393627785&h2=reference).
    - There is a lib for [Data_encoding](https://gitlab.com/nomadic-labs/data-encoding) written by Nomadic Lab

